﻿using System.Collections.Generic;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.Models;
using FourthMVCAppMy.Models.Ads;
using FourthMVCAppMy.Models.Comments;

namespace FourthMVCAppMy.Services.Ads.Contracts
{
    public interface IAdService
    {
        IEnumerable<GalleryImageModel> UploadImages(GalleryImageCreateModel model);
        CommentModel AddComment(CommentCreateModel model, User user);
        AdModel GetAdById(int recordId);
        public AdCreateModel GetAdCreateModel();
        void CreateAd(AdCreateModel model, int currentUserId);
        List<AdModel> GetAdFilterModel(AdFilterModel model);
    }
}
