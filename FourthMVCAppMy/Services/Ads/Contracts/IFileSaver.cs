﻿using FourthMVCAppMy.DAL.Entities;
using Microsoft.AspNetCore.Http;

namespace FourthMVCAppMy.Services.Ads.Contracts
{
    public interface IFileSaver
    {
        void SaveFile(Ad record, IFormFile formFile);
    }
}
