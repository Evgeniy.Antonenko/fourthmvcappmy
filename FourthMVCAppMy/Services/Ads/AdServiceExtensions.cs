﻿using System.Collections.Generic;
using System.Linq;
using FourthMVCAppMy.DAL.Entities;

namespace FourthMVCAppMy.Services.Ads
{
    public static class AdServiceExtensions
    {
        public static IEnumerable<Ad> BySearchKey(this IEnumerable<Ad> ads, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                ads = ads.Where(a => a.Title.Contains(searchKey) || a.Description.Contains(searchKey));

            return ads;
        }

        
        public static IEnumerable<Ad> ByPriceFrom(this IEnumerable<Ad> ads, decimal? priceFrom)
        {
            if (priceFrom.HasValue)
                ads = ads.Where(a => a.Price >= priceFrom.Value);

            return ads;
        }

        public static IEnumerable<Ad> ByPriceTo(this IEnumerable<Ad> ads, decimal? priceTo)
        {
            if (priceTo.HasValue)
                ads = ads.Where(a => a.Price <= priceTo.Value);

            return ads;
        }
    }
}
