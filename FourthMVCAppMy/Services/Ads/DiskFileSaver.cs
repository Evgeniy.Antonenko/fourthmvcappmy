﻿using System;
using System.IO;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.Services.Ads.Contracts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace FourthMVCAppMy.Services.Ads
{
    public class DiskFileSaver : IFileSaver
    {
        private readonly IWebHostEnvironment _hostEnvironment;

        public DiskFileSaver(IWebHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
        }

        public void SaveFile(Ad ad, IFormFile formFile)
        {
            var fileFullName = formFile.FileName;
            var fileId = Guid.NewGuid();
            var fileName = Path.GetFileNameWithoutExtension(fileFullName);
            var fileExtension = Path.GetExtension(fileFullName);

            string filePath = $"/files/{fileName}-{fileId}{fileExtension}";
            ad.ImagePath = filePath;
            ad.RecordImageType = RecordImageType.Disk;
            using (var fileStream = new FileStream(_hostEnvironment.WebRootPath + filePath, FileMode.Create))
            {
                formFile.CopyTo(fileStream);
            }
        }
    }
}
