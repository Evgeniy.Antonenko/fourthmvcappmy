﻿using System.IO;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.Services.Ads.Contracts;
using Microsoft.AspNetCore.Http;

namespace FourthMVCAppMy.Services.Ads
{
    public class DbFilesSaver : IFileSaver
    {
        public void SaveFile(Ad ad, IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                ad.Image = binaryReader.ReadBytes((int)formFile.Length);
                ad.RecordImageType = RecordImageType.Db;
            }
        }

        public byte[] GetImageBytes(IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                return binaryReader.ReadBytes((int)formFile.Length);
            }
        }
    }
}
