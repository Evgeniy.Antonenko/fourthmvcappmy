﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FourthMVCAppMy.DAL;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.Models;
using FourthMVCAppMy.Models.Ads;
using FourthMVCAppMy.Models.Comments;
using FourthMVCAppMy.Services.Ads.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FourthMVCAppMy.Services.Ads
{
    public class AdService : IAdService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileSaver _fileSaver;
        private readonly DbFilesSaver _dbFileSaver;

        public AdService(IUnitOfWorkFactory unitOfWorkFactory, IFileSaver fileSaver, DbFilesSaver dbFileSaver)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (fileSaver == null)
                throw new ArgumentNullException(nameof(fileSaver));
            if (dbFileSaver == null)
                throw new ArgumentNullException(nameof(dbFileSaver));

            _unitOfWorkFactory = unitOfWorkFactory;
            _fileSaver = fileSaver;
            _dbFileSaver = dbFileSaver;
        }
        
        public List<AdModel> GetAdFilterModel(AdFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Ad> ads = unitOfWork.Ads.GetFullAdWithAuthorAndCategoty().ToList();

                ads = ads
                    .BySearchKey(model.SearchKey)
                    .ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo);

                
                var models = Mapper.Map<List<AdModel>>(ads.ToList());

                return models;
            }
        }

        public AdCreateModel GetAdCreateModel()
        {
            return new AdCreateModel()
            {
                CategoriesSelect = GetCategoriesSelect()
            };
        }

        public void CreateAd(AdCreateModel model, int currentUserId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = Mapper.Map<Ad>(model);
                ad.AuthorId = currentUserId;

                _fileSaver.SaveFile(ad, model.Image);

                unitOfWork.Ads.Create(ad);
            }
        }

        public AdModel GetAdById(int adId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetAdByIdFull(adId);

                return Mapper.Map<AdModel>(ad);
            }
        }

        public CommentModel AddComment(CommentCreateModel model, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var comment = Mapper.Map<Comment>(model);
                comment.AuthorId = user.Id;
                comment.DateOfPublication = DateTime.Now;
                unitOfWork.Comments.Create(comment);
                comment.Author = user;
                return Mapper.Map<CommentModel>(comment);
            }
        }

        public SelectList GetCategoriesSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                return new SelectList(categories, nameof(Category.Id), nameof(Category.CategoryName));
            }
        }

        public IEnumerable<GalleryImageModel> UploadImages(GalleryImageCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                foreach (var image in model.Images)
                {
                    var galleryImage = new GalleryImage()
                    {
                        AdId = model.AdId,
                        Name = image.FileName,
                        Image = _dbFileSaver.GetImageBytes(image)
                    };
                    unitOfWork.GalleryImages.Create(galleryImage);
                    yield return new GalleryImageModel()
                    {
                        Image = galleryImage.Image,
                        Name = galleryImage.Name
                    };
                }
            }
        }
    }
}
