﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FourthMVCAppMy.Models
{
    public class GalleryImageCreateModel
    {
        [Required]
        public int AdId { get; set; }

        public IFormFileCollection Images { get; set; }
    }
}
