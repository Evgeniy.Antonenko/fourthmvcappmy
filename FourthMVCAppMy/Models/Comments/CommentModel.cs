﻿using System.ComponentModel.DataAnnotations;

namespace FourthMVCAppMy.Models.Comments
{
    public class CommentModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата публикации сообщения")]
        public string DateOfPublication { get; set; }

        [Display(Name = "Коментарий")]
        public string CommentContent { get; set; }

        [Display(Name = "Автор коментария")]
        public string Author { get; set; }

        [Display(Name = "Объявление")]
        public string Ad { get; set; }
    }
}
