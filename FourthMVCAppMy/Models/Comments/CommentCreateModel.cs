﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace FourthMVCAppMy.Models.Comments
{
    public class CommentCreateModel
    {
        [Required]
        [JsonProperty(propertyName: "commentContent")]
        public string CommentContent { get; set; }

        [Required]
        [JsonProperty(propertyName: "adId")]
        public int AdId { get; set; }
    }
}
