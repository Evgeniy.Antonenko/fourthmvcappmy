﻿using System.ComponentModel.DataAnnotations;

namespace FourthMVCAppMy.Models.Categories
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [Display(Name = "Категория")]
        [Required]
        public string CategoryName { get; set; }
    }
}
