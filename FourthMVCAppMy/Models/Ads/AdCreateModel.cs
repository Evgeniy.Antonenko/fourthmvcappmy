﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FourthMVCAppMy.Models.Ads
{
    public class AdCreateModel
    {
        [Display(Name = "Дата создания объявления")]
        [Required]
        public string DateOfСreation { get; set; }

        [Display(Name = "Заголовок объявления")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Цена")]
        [Required]
        public decimal Price { get; set; }

        [Display(Name = "Описание объявления")]
        [Required]
        public string Description { get; set; }

        public int CategoryId { get; set; }

        [Display(Name = "Категория")]
        [Required]
        public string Categories { get; set; }

        public SelectList CategoriesSelect { get; set; }

        [Display(Name = "Контактное лицо")]
        [Required]
        public string ContactName { get; set; }

        [Display(Name = "Контакты")]
        [Required]
        public string ContactPhone { get; set; }

        [Display(Name = "Изображение")]
        [Required]
        public IFormFile Image { get; set; }
    }
}
