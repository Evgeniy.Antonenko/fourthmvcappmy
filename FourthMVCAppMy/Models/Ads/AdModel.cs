﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.Models.Comments;

namespace FourthMVCAppMy.Models.Ads
{
    public class AdModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата создания объявления")]
        [Required]
        public string DateOfСreation { get; set; }

        [Display(Name = "Заголовок объявления")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "\"Цена\" должна быть указана")]
        public decimal Price { get; set; }

        [Display(Name = "Описание объявления")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Имя автора объявления")]
        [Required]
        public string ContactName { get; set; }

        [Display(Name = "Контактный телефон")]
        [Required]
        public string ContactPhone { get; set; }

        public int AuthorId { get; set; }

        [Display(Name = "Автор объявления")]
        [Required]
        public string Author { get; set; }

        public int CategoryId { get; set; }

        [Display(Name = "Категория")]
        [Required]
        public string Categories { get; set; }

        public List<CommentModel> Comments { get; set; }

        public byte[] Image { get; set; }

        public string ImagePath { get; set; }

        public RecordImageType RecordImageType { get; set; }

        public List<GalleryImageModel> Images { get; set; }
    }
}
