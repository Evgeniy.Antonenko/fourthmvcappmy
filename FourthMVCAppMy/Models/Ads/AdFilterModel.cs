﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FourthMVCAppMy.Models.Ads
{
    public class AdFilterModel
    {
        [Display(Name = "По автору темы")]
        public string Author { get; set; }

        [Display(Name = "По ключевому слову")]
        public string SearchKey { get; set; }

        [Display(Name = "Цена от")]
        public decimal? PriceFrom { get; set; }

        [Display(Name = "Цена до")]
        public decimal? PriceTo { get; set; }
        
        public AdModel AdModel { get; set; }

        public List<AdModel> Ads { get; set; }
    }
}
