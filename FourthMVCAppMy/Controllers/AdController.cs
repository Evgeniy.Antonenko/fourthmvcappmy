﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.Models;
using FourthMVCAppMy.Models.Ads;
using FourthMVCAppMy.Models.Comments;
using FourthMVCAppMy.Services.Ads.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FourthMVCAppMy.Controllers
{
    public class AdController : Controller
    {
        private readonly IAdService _adService;
        private readonly UserManager<User> _userManager;

        public AdController(IAdService adService, UserManager<User> userManager)
        {
            if (adService == null)
                throw new ArgumentNullException(nameof(adService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _adService = adService;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index(AdFilterModel model)
        {
            try
            {
                var recordModels = _adService.GetAdFilterModel(model);

                model.Ads = recordModels;

                return View(model);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        public IActionResult AdCreate()
        {
            var model = _adService.GetAdCreateModel();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AdCreate(AdCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);

                _adService.CreateAd(model, currentUser.Id);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Details(int adId)
        {
            var adModel = _adService.GetAdById(adId);

            return View(adModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddComment(CommentCreateModel model)
        {
            try
            {
                var currentUser = await _userManager.GetUserAsync(User);
                var commentModel = _adService.AddComment(model, currentUser);

                return Ok(commentModel);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult Upload(GalleryImageCreateModel model)
        {
            try
            {
                _adService.UploadImages(model);
                return RedirectToAction("Details", new { recordId = model.AdId });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult UploadAjax(int adId)
        {
            try
            {
                GalleryImageCreateModel model = new GalleryImageCreateModel()
                {
                    AdId = adId,
                    Images = Request.Form.Files
                };

                var imageModels = _adService.UploadImages(model);

                return Ok(imageModels.ToList());
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
