﻿using System;
using AutoMapper;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.Models;
using FourthMVCAppMy.Models.Ads;
using FourthMVCAppMy.Models.Comments;

namespace FourthMVCAppMy
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateGalleryImageToModel();
            CreateAddCommentRequestToCommentMap();
            CreateCommentToCommentModelMap();
            CreateAdToAdModelMap();
            CreateAdCreateModelToAd();
        }

        private void CreateGalleryImageToModel()
        {
            CreateMap<GalleryImage, GalleryImageModel>();
        }

        private void CreateAddCommentRequestToCommentMap()
        {
            CreateMap<CommentCreateModel, Comment>()
                .ForMember(target => target.CommentContent,
                    src => src.MapFrom(p => p.CommentContent));
        }

        private void CreateCommentToCommentModelMap()
        {
            CreateMap<Comment, CommentModel>()
                .ForMember(target => target.DateOfPublication,
                    src => src.MapFrom(p => p.DateOfPublication.ToString("F")))
                .ForMember(target => target.Author,
                    src => src.MapFrom(p => p.Author.UserName));
        }

        public void CreateAdToAdModelMap()
        {
            CreateMap<Ad, AdModel>()
                .ForMember(target => target.DateOfСreation,
                    src => src.MapFrom(p => p.DateOfСreation.ToString("F")))
                .ForMember(target => target.Author,
                    src => src.MapFrom(p => p.Author.UserName))
                .ForMember(target => target.Categories,
                    src => src.MapFrom(p => p.Categories.CategoryName))
                .ForMember(target => target.Images,
                    src => src.MapFrom(p => p.GalleryImages));
        }

        public void CreateAdCreateModelToAd()
        {
            CreateMap<AdCreateModel, Ad>()
                .ForMember(target => target.DateOfСreation,
                    src => src.MapFrom(p => DateTime.Now))
                .ForMember(target => target.Image,
                    src => src.Ignore());
        }
    }
}
