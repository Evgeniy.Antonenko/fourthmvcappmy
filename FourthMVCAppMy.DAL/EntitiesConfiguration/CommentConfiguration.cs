﻿using FourthMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FourthMVCAppMy.DAL.EntitiesConfiguration
{
    public class CommentConfiguration : BaseEntityConfiguration<Comment>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Comment> builder)
        {
            builder
                .Property(co => co.CommentContent)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(co => co.DateOfPublication)
                .IsRequired();
            builder
                .HasIndex(co => co.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasOne(co => co.Author)
                .WithMany(co => co.Comments)
                .HasForeignKey(co => co.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(co => co.Ad)
                .WithMany(co => co.Comments)
                .HasForeignKey(co => co.AdId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
