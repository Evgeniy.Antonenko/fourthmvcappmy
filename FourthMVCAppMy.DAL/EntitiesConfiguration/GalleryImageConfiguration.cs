﻿using FourthMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FourthMVCAppMy.DAL.EntitiesConfiguration
{
    public class GalleryImageConfiguration : BaseEntityConfiguration<GalleryImage>
    {
        protected override void ConfigureForeignKeys(EntityTypeBuilder<GalleryImage> builder)
        {
            builder
                .HasOne(g => g.Ad)
                .WithMany(g => g.GalleryImages)
                .HasForeignKey(g => g.AdId)
                .IsRequired();
        }
    }
}
