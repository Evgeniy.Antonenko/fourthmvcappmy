﻿using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.DAL.EntitiesConfiguration.Contracts;

namespace FourthMVCAppMy.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Ad> AdConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<Comment> CommentConfiguration { get; }
        public IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            AdConfiguration = new AdConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            CommentConfiguration = new CommentConfiguration();
            GalleryImageConfiguration = new GalleryImageConfiguration();
        }
    }
}
