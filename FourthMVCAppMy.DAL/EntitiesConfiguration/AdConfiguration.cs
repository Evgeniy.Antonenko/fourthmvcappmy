﻿using FourthMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FourthMVCAppMy.DAL.EntitiesConfiguration
{
    public class AdConfiguration : BaseEntityConfiguration<Ad>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Ad> builder)
        {
            builder
                .Property(a => a.Title)
                .HasMaxLength(400)
                .IsRequired();
            builder.Property(a => a.Price)
                .HasDefaultValue(0.0M)
                .IsRequired();
            builder
                .Property(a => a.Description)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(a => a.DateOfСreation)
                .IsRequired();
            builder
                .Property(a => a.ContactName)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(a => a.ContactPhone)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .HasIndex(a => a.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Ad> builder)
        {
            builder
                .HasOne(a => a.Author)
                .WithMany(a => a.Ads)
                .HasForeignKey(a => a.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(a => a.Categories)
                .WithMany(a => a.Ads)
                .HasForeignKey(a => a.CategoryId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasMany(a => a.Comments)
                .WithOne(a => a.Ad)
                .HasForeignKey(a => a.AdId)
                .IsRequired();
            builder
                .HasMany(r => r.GalleryImages)
                .WithOne(r => r.Ad)
                .HasForeignKey(r => r.AdId);
        }
    }
}
