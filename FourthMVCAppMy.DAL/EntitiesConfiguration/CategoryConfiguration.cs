﻿using FourthMVCAppMy.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FourthMVCAppMy.DAL.EntitiesConfiguration
{
    public class CategoryConfiguration : BaseEntityConfiguration<Category>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Category> builder)
        {
            builder
                .Property(c => c.CategoryName)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .HasIndex(c => c.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Category> builder)
        {
            builder
                .HasMany(c => c.Ads)
                .WithOne(c => c.Categories)
                .HasForeignKey(c => c.CategoryId)
                .IsRequired();
        }
    }
}
