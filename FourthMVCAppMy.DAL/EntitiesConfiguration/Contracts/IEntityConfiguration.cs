﻿using System;
using FourthMVCAppMy.DAL.Entities.Contracts;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FourthMVCAppMy.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
