﻿using FourthMVCAppMy.DAL.Entities;

namespace FourthMVCAppMy.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Ad> AdConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Comment> CommentConfiguration { get; }
        IEntityConfiguration<GalleryImage> GalleryImageConfiguration { get; }

    }
}
