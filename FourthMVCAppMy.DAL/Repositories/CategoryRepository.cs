﻿using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.DAL.Repositories.Contracts;

namespace FourthMVCAppMy.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
