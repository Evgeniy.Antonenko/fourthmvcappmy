﻿using System.Collections.Generic;
using System.Linq;
using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FourthMVCAppMy.DAL.Repositories
{
    public class AdRepository : Repository<Ad>, IAdRepository
    {
        public AdRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Ads;
        }

        public IEnumerable<Ad> GetFullAdWithAuthorAndCategoty()
        {
            return entities.Include(a => a.Author).Include(a => a.Categories);
        }

        public Ad GetAdByIdFull(int adId)
        {
            return entities.Include(a => a.Author).Include(a => a.GalleryImages)
                .Include(a => a.Comments).ThenInclude(a => a.Author)
                .FirstOrDefault(a => a.Id == adId);
        }
    }
}
