﻿using System.Collections.Generic;
using FourthMVCAppMy.DAL.Entities;

namespace FourthMVCAppMy.DAL.Repositories.Contracts
{
    public interface IAdRepository : IRepository<Ad>
    {
        IEnumerable<Ad> GetFullAdWithAuthorAndCategoty();
        Ad GetAdByIdFull(int adId);
    }
}
