﻿using FourthMVCAppMy.DAL.Entities;

namespace FourthMVCAppMy.DAL.Repositories.Contracts
{
    public interface IGalleryImageRepository : IRepository<GalleryImage>
    {
    }
}
