﻿using FourthMVCAppMy.DAL.Entities;

namespace FourthMVCAppMy.DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
