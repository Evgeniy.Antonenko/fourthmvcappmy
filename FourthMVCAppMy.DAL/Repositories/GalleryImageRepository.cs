﻿using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.DAL.Repositories.Contracts;

namespace FourthMVCAppMy.DAL.Repositories
{
    public class GalleryImageRepository : Repository<GalleryImage>, IGalleryImageRepository
    {
        public GalleryImageRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.GalleryImages;
        }
    }
}
