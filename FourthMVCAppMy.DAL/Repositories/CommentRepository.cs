﻿using FourthMVCAppMy.DAL.Entities;
using FourthMVCAppMy.DAL.Repositories.Contracts;

namespace FourthMVCAppMy.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Comments;
        }
    }
}
