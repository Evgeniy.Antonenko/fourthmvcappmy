﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FourthMVCAppMy.DAL.Entities.Contracts;
using FourthMVCAppMy.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace FourthMVCAppMy.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private ApplicationDbContext _context;
        protected DbSet<T> entities;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<T> CreateAsync(T entity)
        {
            var entityEntry = entities.Add(entity);
            await _context.SaveChangesAsync();
            return entityEntry.Entity;
        }

        public T Create(T entity)
        {
            var entityEntry = entities.Add(entity);
            _context.SaveChanges();
            return entityEntry.Entity;
        }

        public T GetById(int id)
        {
            return entities.FirstOrDefault(e => e.Id == id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return entities;
        }

        public T Update(T entity)
        {
            var entityEntry = _context.Update(entity);
            _context.SaveChanges();
            return entityEntry.Entity;
        }

        public void Remove(T entity)
        {
            entities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
