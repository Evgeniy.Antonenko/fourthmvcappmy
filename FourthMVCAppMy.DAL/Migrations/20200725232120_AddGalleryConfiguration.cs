﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FourthMVCAppMy.DAL.Migrations
{
    public partial class AddGalleryConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GalleryImage_Ads_AdId",
                table: "GalleryImage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GalleryImage",
                table: "GalleryImage");

            migrationBuilder.RenameTable(
                name: "GalleryImage",
                newName: "GalleryImages");

            migrationBuilder.RenameIndex(
                name: "IX_GalleryImage_AdId",
                table: "GalleryImages",
                newName: "IX_GalleryImages_AdId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GalleryImages",
                table: "GalleryImages",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_GalleryImages_Ads_AdId",
                table: "GalleryImages",
                column: "AdId",
                principalTable: "Ads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GalleryImages_Ads_AdId",
                table: "GalleryImages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GalleryImages",
                table: "GalleryImages");

            migrationBuilder.RenameTable(
                name: "GalleryImages",
                newName: "GalleryImage");

            migrationBuilder.RenameIndex(
                name: "IX_GalleryImages_AdId",
                table: "GalleryImage",
                newName: "IX_GalleryImage_AdId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GalleryImage",
                table: "GalleryImage",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_GalleryImage_Ads_AdId",
                table: "GalleryImage",
                column: "AdId",
                principalTable: "Ads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
