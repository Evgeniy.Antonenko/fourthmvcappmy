﻿namespace FourthMVCAppMy.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
