﻿using System;
using FourthMVCAppMy.DAL.Repositories;
using FourthMVCAppMy.DAL.Repositories.Contracts;

namespace FourthMVCAppMy.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IAdRepository Ads { get; set; }
        public ICategoryRepository Categories { get; set; }
        public ICommentRepository Comments { get; set; }
        public IGalleryImageRepository GalleryImages { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Ads = new AdRepository(context);
            Categories = new CategoryRepository(context);
            Comments = new CommentRepository(context);
            GalleryImages = new GalleryImageRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
