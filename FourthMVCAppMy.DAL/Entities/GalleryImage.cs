﻿using FourthMVCAppMy.DAL.Entities.Contracts;

namespace FourthMVCAppMy.DAL.Entities
{
    public class GalleryImage : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public int AdId { get; set; }
        public Ad Ad { get; set; }
    }
}
