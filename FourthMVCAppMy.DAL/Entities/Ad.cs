﻿using System;
using System.Collections.Generic;
using FourthMVCAppMy.DAL.Entities.Contracts;

namespace FourthMVCAppMy.DAL.Entities
{
    public class Ad : IEntity
    {
        public int Id { get; set; }

        public DateTime DateOfСreation { get; set; }

        public string Title { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public string ContactName { get; set; }

        public string ContactPhone { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }
        
        public int CategoryId { get; set; }

        public Category Categories { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public byte[] Image { get; set; }

        public string ImagePath { get; set; }

        public RecordImageType RecordImageType { get; set; }

        public ICollection<GalleryImage> GalleryImages { get; set; }

        public int Up { get; set; }
    }
}
