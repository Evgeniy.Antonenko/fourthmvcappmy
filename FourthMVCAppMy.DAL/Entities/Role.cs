﻿using FourthMVCAppMy.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace FourthMVCAppMy.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
