﻿using System;
using FourthMVCAppMy.DAL.Entities.Contracts;

namespace FourthMVCAppMy.DAL.Entities
{
    public class Comment : IEntity
    {
        public int Id { get; set; }

        public DateTime DateOfPublication { get; set; }

        public string CommentContent { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }

        public int AdId { get; set; }

        public Ad Ad { get; set; }
    }
}
