﻿
namespace FourthMVCAppMy.DAL.Entities.Contracts
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
