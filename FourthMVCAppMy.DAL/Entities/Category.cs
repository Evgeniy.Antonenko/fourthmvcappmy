﻿using System.Collections.Generic;
using FourthMVCAppMy.DAL.Entities.Contracts;

namespace FourthMVCAppMy.DAL.Entities
{
    public class Category : IEntity
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        public ICollection<Ad> Ads { get; set; }
    }
}
