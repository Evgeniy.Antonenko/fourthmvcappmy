﻿using System.Collections.Generic;
using FourthMVCAppMy.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace FourthMVCAppMy.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Ad> Ads { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}
